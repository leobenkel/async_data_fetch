require 'async_data_fetch/acts_as_async'
require 'async_data_fetch/view_helpers'

module AsyncDataFetch
	require 'async_data_fetch/engine'

	I18n.available_locales = [:en, :fr]
	I18n.default_locale = :en
end
