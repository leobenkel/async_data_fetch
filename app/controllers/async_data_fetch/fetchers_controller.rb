require_dependency "async_data_fetch/application_controller"

module AsyncDataFetch
	class FetchersController < ApplicationController # ::ApplicationController

		def get_async_data
			model = fetcher_params[:model_name].classify.constantize
			if model == nil then
				respond_to do |format|
					format.json { render :json => { :error => t( "async_data_fetch.Wrong_model")}, :status => 422 }
				end
				return
			end

			item = model.find_by(id: fetcher_params[:model_id])
			if item == nil then
				respond_to do |format|
					format.json { render :json => { :error => t( "async_data_fetch.Wrong_id")}, :status => 422}
				end
				return
			end

			result = nil
			if item != nil && model.async_data_fetch_available_methods != nil && model.async_data_fetch_available_methods.include?(fetcher_params[:model_method]) then
				result = item.send(fetcher_params[:model_method])
				respond_to do |format|
					format.json { render :json => { :success => result }, :status => 200 }
				end
				return
			end

			respond_to do |format|
				format.json { render :json => { :error => t( "async_data_fetch.Invalid_method") }, :status => 422 }
			end
			return
		end

		private
		def fetcher_params
			params.require(:model_method)
			params.require(:model_id)
			params.require(:model_name)
			return params
		end

	end
end

