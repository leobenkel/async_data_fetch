require 'test_helper'

	class FetchersControllerTest < ActionController::TestCase
		def setup
			@routes = AsyncDataFetch::Engine.routes
			@controller = AsyncDataFetch::FetchersController.new
		end

		def test_route
			assert_routing get_async_data_path, {controller: "fetchers", action: "get_async_data"}
		end


		def test_get_data_async
			post get_async_data_path
		end
	end
